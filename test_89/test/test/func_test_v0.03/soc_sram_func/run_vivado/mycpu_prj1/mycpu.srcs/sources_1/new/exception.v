`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/01/02 19:56:17
// Design Name: 
// Module Name: exception
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module exception(
    input wire clk,rst,
    input wire [31:0] pc_E,
    input wire pc_error_false,
    input wire is_in_delayslotE,
    input wire [5:0] interrupt,
    input wire [8:0] exception_signs,
    input wire [4:0] rdE,
    input wire [31:0] bad_addr,wr_dataE,
    output wire flush_exception,
    output reg [31:0] new_pc,
    output wire[31:0] rd_dataE
    //-------------------------------
//    output wire [31:0] addr,
//    output wire  ,
///    output wire  ,
    //----------------------------
    );

    wire [31:0] except_typeE;
    wire [31:0] bad_pc;
    wire [31:0] epc,status_o,cause_o;

    
    wire int_en,pc_error;
    assign flush_exception = (except_typeE != 32'h00000000) ;

    assign pc_error = pc_E[1:0] != 2'b00;
    assign int_en = !status_o[1] & status_o[0] & (|(status_o[15:8] & cause_o[15:8]));  //--------重点

    assign except_typeE = (int_en) ? `CP0_EXCEPTTYPE_INT : 
                          (exception_signs[8] | pc_error)  ? `CP0_EXCEPTTYPE_AdEL : 
                          (exception_signs[7])  ? `CP0_EXCEPTTYPE_RI : 
                          (exception_signs[6])  ? `CP0_EXCEPTTYPE_OverFlow : 
                          (exception_signs[5])  ? `CP0_EXCEPTTYPE_Break : 
                          (exception_signs[4])  ? `CP0_EXCEPTTYPE_Syscall : 
                          (exception_signs[3])  ? `CP0_EXCEPTTYPE_Eret : 
                          (exception_signs[2])  ? `CP0_EXCEPTTYPE_AdES : 32'h00000000;

    assign bad_pc = (pc_error) ? pc_E : bad_addr;


    
    //exception_signs: 读地�??错误,指令不存�??,溢出,break,syscall,eret,写地�??错误,mtc0,mfc0
    
    cp0_reg cp0(
    .clk(clk),.rst(rst),
    .we_i(exception_signs[1]), 
	.waddr_i(rdE), //要写入的地址
	.raddr_i(rdE), //要读出的地址
	.data_i(wr_dataE),  //要写入的数据
    .int_i(6'b000000),
    .excepttype_i(except_typeE),
	.current_inst_addr_i(pc_E),
	.is_in_delayslot_i(is_in_delayslotE),
	.bad_addr_i(bad_pc),

    .data_o(rd_dataE), // 读出的cp0对应索引的数�?
    .epc_o(epc),
    .status_o(status_o),
    .cause_o(cause_o)
    );

    //------------------------------------------------------------------------------------------------

    always @(*) begin
        case(except_typeE)
            `CP0_EXCEPTTYPE_INT,`CP0_EXCEPTTYPE_AdEL,`CP0_EXCEPTTYPE_RI,
            `CP0_EXCEPTTYPE_OverFlow,`CP0_EXCEPTTYPE_Break,`CP0_EXCEPTTYPE_Syscall,
            `CP0_EXCEPTTYPE_AdES :
                        new_pc <=  `EXC_ENTRY_PC;
            `CP0_EXCEPTTYPE_Eret :
                        new_pc <= epc;
            default : new_pc <= pc_E;
        endcase
    end






    //------------------------------------------------------------------------------------------------







endmodule
