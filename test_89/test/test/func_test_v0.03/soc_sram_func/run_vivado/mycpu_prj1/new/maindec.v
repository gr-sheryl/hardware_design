`timescale 1ns / 1ps
`include "defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 18:03:57
// Design Name: 
// Module Name: maindec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module maindec(
	input wire[5:0] op,
    input wire[5:0] funct,
    input wire [4:0] rs,rt,
    
	output wire memtoreg,memwrite,
	output wire branch,alusrc,
	output wire regdst,regwrite,
	output wire jump,
    output wire jal,jr,bal,
    output reg [9:0] signs
    );
    
	
    assign {jump,regwrite,regdst,alusrc,branch,memwrite,memtoreg,jal,jr,bal}= signs;
    //1      jump跳转�????0为不跳转�????1为跳�????
    //2      regwrite寄存器写使能�????1为写�????0为不�????
    //3      regdst：寄存器写入位置�????0为写入rt�????1为写入rd
    //4      alusrc：�?�择alu的操作数，是 rd（为0�???? 还是 立即数（�????1�????
    //5      branch：是否为分支指令�????0为不是，1为是�????
    //6      memwrite：数据存储器写使能（0为不写，1为写�????
    //7      memtoreg：写入寄存器的�?�是哪一个（0为ALU运算结果�????1�???? Data—�?�mem读出的结果）
    //8      jal
    //9      jr
    //10     bal
    //     load    0_1_0_1_0_0_0_000
    //     store   0_0_0_1_0_1_0_000
    always @ (*) begin
        case (op)    
            `EXE_NOP:  
                
                case(funct) 
                `EXE_JALR: signs <= 10'b0100000_010;
                `EXE_JR: signs <= 10'b1000000_010; 
                default : signs <= 10'b0110000_000; //逻辑指令(add,sub,and,or,xor,nor)
                                                //移位指令(sll,srl,sra,sllv,srlv,srav)  
                endcase
            `EXE_REGIMM_INST: case(rt)
                    `EXE_BLTZ: signs <= 10'b0000100_000;
                    `EXE_BLTZAL: signs <= 10'b0100100_001;
                    `EXE_BGEZ: signs <= 10'b0000100_000;
                    `EXE_BGEZAL: signs <= 10'b0100100_001;
                endcase
            
        
            `EXE_LUI,`EXE_ANDI,`EXE_ORI,
            `EXE_XORI,`EXE_ADDI,`EXE_ADDIU,
            `EXE_SLTI,`EXE_SLTIU : signs <= 10'b0101000_000;


            //-----------------------------------
            6'b010000 : case(rs)
                5'b00100  : signs <= 10'b0000000_000; //mtc0
                5'b00000  : signs <= 10'b0100000_000; //mfc0
                default:  signs <= 10'b0000000_000;
                endcase
            //-----------------------------------

            //------------jump,branch-------------

            `EXE_J : signs <= 10'b1000000_000;
            `EXE_JAL: signs <= 10'b0100000_100;

            `EXE_BEQ: signs <= 10'b0000100_000;
            `EXE_BNE: signs <= 10'b0000100_000;
            `EXE_BGTZ: signs <= 10'b0000100_000;
            `EXE_BLEZ: signs <= 10'b0000100_000;

            //------------jump,branch-------------

            //----------------load/store------------------
            `EXE_LB : signs <= 10'b0_1_0_1_0_0_1_000;
            `EXE_LBU: signs <= 10'b0_1_0_1_0_0_1_000;
            `EXE_LH : signs <= 10'b0_1_0_1_0_0_1_000;
            `EXE_LHU: signs <= 10'b0_1_0_1_0_0_1_000;
            `EXE_LW: signs <=  10'b0_1_0_1_0_0_1_000;

            `EXE_SB: signs <=  10'b0_0_0_1_0_1_0_000;
            `EXE_SH: signs <=  10'b0_0_0_1_0_1_0_000;
            `EXE_SW: signs <=  10'b0_0_0_1_0_1_0_000;
            //----------------load/store------------------

            default: begin
                signs <= 10'b0000000_000;
            end 
        endcase
    end
endmodule


