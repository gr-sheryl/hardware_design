`timescale 1ns / 1ps
`include "defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/01/01 16:21:36
// Design Name: 
// Module Name: reg_data
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reg_data(
 input [31:0] resultW_tmp,
    input [1:0] addrW,
    input [7:0] alucontrolW,

    output reg [31:0] resultW
    );

always @(*) begin
    case (alucontrolW)
        `EXE_LB_OP: 
        begin
            // resultW <=resultW_tmp;
            if( addrW == 2'b11 ) 
                resultW <= {{24{resultW_tmp[31]}},resultW_tmp[31:24]};
            else if( addrW == 2'b10 ) 
                resultW <= {{24{resultW_tmp[23]}},resultW_tmp[23:16]};
            else if( addrW == 2'b01 ) 
                resultW <= {{24{resultW_tmp[15]}},resultW_tmp[15:8]};
            else if( addrW == 2'b00 ) 
                resultW <= {{24{resultW_tmp[7]}},resultW_tmp[7:0]};
		end

        `EXE_LBU_OP:
        begin
            // resultW<=resultW_tmp;
            if( addrW == 2'b11 ) 
                resultW <= {{24{1'b0}},resultW_tmp[31:24]};
            else if( addrW == 2'b10 ) 
                resultW <= {{24{1'b0}},resultW_tmp[23:16]};
            else if( addrW == 2'b01 ) 
                resultW <= {{24{1'b0}},resultW_tmp[15:8]};
            else if( addrW == 2'b00 ) 
                resultW <= {{24{1'b0}},resultW_tmp[7:0]};
		end

        `EXE_LH_OP:
        begin
            if( addrW == 2'b10 ) 
                resultW <= {{16{resultW_tmp[31]}},resultW_tmp[31:16]};
            else if( addrW == 2'b00 ) 
                resultW <= {{16{resultW_tmp[15]}},resultW_tmp[15:0]};
        end

        `EXE_LHU_OP:
        begin
            if( addrW == 2'b10 ) 
                resultW <= {{16{1'b0}},resultW_tmp[31:16]};
            else if( addrW == 2'b00 ) 
                resultW <= {{16{1'b0}},resultW_tmp[15:0]};
        end

        `EXE_LW_OP: resultW <= resultW_tmp;

        default: resultW<=resultW_tmp;
    endcase
end


endmodule