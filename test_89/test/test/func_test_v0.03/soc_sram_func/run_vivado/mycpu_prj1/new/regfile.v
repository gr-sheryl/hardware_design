`timescale 1ns / 1ps
`include "defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 17:42:45
// Design Name: 
// Module Name: regfile
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module regfile(
	input wire clk,
	input wire rst,
	input wire we3,
	input wire[4:0] ra1,ra2,wa3,
	input wire[31:0] wd3,
	output wire[31:0] rd1,rd2,
	input wire [7:0] alu_op, 
	input wire[1:0] addr
    );

	reg [31:0] rf[31:0];
	integer i = 0; 
	always @(*) begin
		if (rst) begin
		  for (i=0;i<32;i=i+1)
		  begin
			rf[i] <= 0;
		  end
		end
		if(we3) begin
			case (alu_op)
				`EXE_LB_OP: begin
				  if( addr == 2'b11 ) rf[wa3] <= {{24{wd3[31]}},wd3[31:24]};
				  else 
				  if( addr == 2'b10 ) rf[wa3] <= {{24{wd3[23]}},wd3[23:16]};
				  else 
				  if( addr == 2'b01 ) rf[wa3] <= {{24{wd3[15]}},wd3[15:8]};
				  else 
				  if( addr == 2'b00 ) rf[wa3] <= {{24{wd3[7]}},wd3[7:0]};
				end
				`EXE_LBU_OP: begin
				  if( addr == 2'b11 ) rf[wa3] <= {{24{1'b0}},wd3[31:24]};
				  else 
				  if( addr == 2'b10 ) rf[wa3] <= {{24{1'b0}},wd3[23:16]};
				  else 
				  if( addr == 2'b01 ) rf[wa3] <= {{24{1'b0}},wd3[15:8]};
				  else 
				  if( addr == 2'b00 ) rf[wa3] <= {{24{1'b0}},wd3[7:0]};
				end

				`EXE_LH_OP:begin
				  if( addr == 2'b10 ) rf[wa3] <= {{16{wd3[31]}},wd3[31:16]};
				  else 
				  if( addr == 2'b00 ) rf[wa3] <= {{16{wd3[15]}},wd3[15:0]};
				end

				`EXE_LHU_OP:begin
					if( addr == 2'b10 ) rf[wa3] <= {{16{1'b0}},wd3[31:16]};
				  else 
				  	if( addr == 2'b00 ) rf[wa3] <= {{16{1'b0}},wd3[15:0]};
				end

				`EXE_LW_OP: rf[wa3] <= wd3;
				default: rf[wa3] <= wd3;
			endcase
			 	
		end
	end

	assign rd1 = (ra1 != 0) ? rf[ra1] : 0;
	assign rd2 = (ra2 != 0) ? rf[ra2] : 0;
endmodule
