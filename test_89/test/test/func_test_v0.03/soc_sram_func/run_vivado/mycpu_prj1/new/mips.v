`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 12:55:33
// Design Name: 
// Module Name: mips
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mips(
	input wire clk,rst,
	output wire[31:0] pcF,
	input wire[31:0] instrF,
	output wire stallF, 
	output wire memwriteM,
	output wire[31:0] aluoutM,writedataM,
	output wire[31:0] resultW,
	output wire[31:0] srcbE,
	output wire[31:0] srcb2E,
	input wire[31:0] readdataM ,
	output wire[5:0] opD, 
	output wire[5:0] functD,  
	output wire pcsrcD,  
	output wire branchD,  
	output wire jumpD,  
	output wire memtoregE,  
	output wire alusrcE,  
	output wire[4:0] rsD,  
	output wire[4:0] rtD,  
	output wire[4:0] rdD,
	output wire[4:0] rsE,
	output wire[4:0] rtE,
	output wire[4:0] rdE,
	output wire [31:0] aluoutE,
	output wire [63:0] hilo_OW,
	output wire jalD,jrD,balD,equalD,
	output wire [9:0] signs,
	output wire [31:0] pcplus8E,
	output wire jumpE,jrE,jalE,balE,
	output wire [3:0] sel,
	output wire [4:0] writeregW,
	output wire stallD,stallE,
	output wire regwriteW,
	output wire[31:0] reg_true_dataW,
	input wire [5:0] intF,
	output wire flushE,flushD,flushM
    );
	
	wire memtoregM,regdstE,regwriteE,memtoregW;
	wire [7:0] alucontrolE;
	wire stallM;

	controller c(
		clk,rst,
		//decode stage
		opD,functD,rtD,rsD,
		pcsrcD,branchD,equalD,jumpD,
		
		//execute stage
		flushE,
		memtoregE,alusrcE,
		regdstE,regwriteE,	
		alucontrolE,

		//mem stage
		memtoregM,memwriteM,
		regwriteM,
		//write back stage
		memtoregW,regwriteW,
		jalD,jrD,balD,
		signs,stallE,stallM,flushM
		);
	datapath dp(
		clk,rst,
		//fetch stage
		pcF,
		instrF,
		stallF,
		//decode stage
		pcsrcD,branchD,
		jumpD,
		equalD,
		opD,functD,
		rsD,rtD,rdD,
		//execute stage
		memtoregE,
		alusrcE,regdstE,
		regwriteE,
		alucontrolE,
		flushE,
		rsE,rtE,rdE,
		//mem stage
		memtoregM,
		regwriteM,
		aluoutM,writedataM,
		readdataM,
		//writeback stage
		memtoregW,
		regwriteW,
		resultW,
		srcbE,
		srcb2E,
		aluoutE,
		hilo_OW,
		jalD,jrD,balD,
		pcplus8E,
		jumpE,jrE,jalE,balE,
		sel,
		stallE,
		writeregW,
		stallD,
		reg_true_dataW,
		stallM,
		intF,
		flushD,flushM
	    );
	
endmodule