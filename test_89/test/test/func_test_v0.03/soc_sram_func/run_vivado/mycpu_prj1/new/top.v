`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 17:43:34
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input wire clk,
    input wire resetn,  //low active
    input wire [5:0] int,  //interrupt,high active

    output wire        inst_sram_en,
    output wire [3 :0] inst_sram_wen,
    output wire [31:0] inst_sram_addr,
    output wire [31:0] inst_sram_wdata,
    input  wire [31:0] inst_sram_rdata,
    
    output wire        data_sram_en,
    output wire [3 :0] data_sram_wen,
    output wire [31:0] data_sram_addr,
    output wire [31:0] data_sram_wdata,
    input  wire [31:0] data_sram_rdata,

    //debug
    output wire [31:0] debug_wb_pc,
    output wire [3 :0] debug_wb_rf_wen,
    output wire [4 :0] debug_wb_rf_wnum,
    output wire [31:0] debug_wb_rf_wdata

    );

    wire [31:0] aluoutM;
    wire [31:0] writedata;
    wire stallF,stallD,stallE;
    wire rf_wnum;
    wire regwriteW;
    wire [31:0] resultW;
    wire [31:0] wdata_addr;
    wire [31:0] pcF;
    wire memwriteW;
    wire [3:0] sel; 
	mips mips(.clk(clk),.rst(resetn),.pcF(pcF),.instrF(inst_sram_rdata),
	.aluoutM(wdata_addr),.memwriteM(memwriteW),.writedataM(writedata),
	.readdataM(data_sram_rdata),.sel(sel),.writeregW(debug_wb_rf_wnum),.stallF(stallF),.stallD(stallD),.stallE(stallE),
    .regwriteW(regwriteW),.resultW(resultW),.reg_true_dataW(debug_wb_rf_wdata),.intF(int),.flushE(flushE),.flushD(flushD),.flushM(flushM));
	


    assign inst_sram_en = 1'b1;
	assign inst_sram_wen = 4'b0000;
    assign inst_sram_wdata = 32'h00000000;
    mmu mmu(pcF,inst_sram_addr,wdata_addr,data_sram_addr);

    assign data_sram_en = 1'b1;
	assign data_sram_wen = (memwriteW == 1'b0) ? 4'b0000 : sel;
	assign data_sram_wdata = (data_sram_wen == 4'b1100 | data_sram_wen == 4'b0011) ? {2{writedata[15:0]}}  :
						     (data_sram_wen != 4'b0000 & data_sram_wen != 4'b1111) ? {4{writedata[7:0]}} : writedata;


    assign debug_wb_rf_wen = {4{regwriteW}};

    wire [31:0] pcD,pcE,pcM;
    flopenrc #(32)pcFD (clk,resetn,~stallD,flushD,pcF,pcD);
    flopenrc #(32)pcDE (clk,resetn,~stallE,flushE,pcD,pcE);
    floprc #(32)pcEM (clk,resetn,flushM,pcE,pcM);
    flopr #(32)pcMW (clk,resetn,pcM,debug_wb_pc);
    
	
endmodule
