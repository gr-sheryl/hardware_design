`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 13:20:28
// Design Name: 
// Module Name: Hazard
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


 module hazard(
	//fetch stage
	output stallF,
	//decode stage
	input [4:0] rsD,rtD,
	input branchD,
	output forwardaD,forwardbD,
	output stallD,
	//execute stage
	input [4:0] rsE,rtE,
	input [4:0] writeregE,
	input regwriteE,
	input memtoregE,
	output [1:0] forwardaE,forwardbE,
	output flushE,
	//mem stage
	input [4:0] writeregM,
	input regwriteM,
	input memtoregM,
	//write back stage
	input [4:0] writeregW,
	input regwriteW,
	input stall_div,
	output stallE,
	input jrD,
	output wire stallM,
	input wire flush_exception,
	output wire flushD,flushM
    );
    
//putforward
//  if      ((rsE != 0) AND (rsE == WriteRegM) AND RegWriteM)     
//		then 	ForwardAE = 10
//	else if ((rsE != 0) AND (rsE == WriteRegW) AND RegWriteW) 
//		then 	ForwardAE = 01
//	else	    	ForwardAE = 00
    assign forwardaE = ( (rsE != 0) & (rsE == writeregM) & regwriteM )? 2'b10:
                        ( (rsE != 0) & (rsE == writeregW) & regwriteW )? 2'b01 : 2'b00;
    assign forwardbE = ( (rtE != 0) & (rtE == writeregM) & regwriteM )? 2'b10:
                        ( (rtE != 0) & (rtE == writeregW) & regwriteW )? 2'b01 : 2'b00;

//pipeline stall
//    lwstall = ((rsD==rtE) OR (rtD==rtE)) AND MemtoRegE
//    StallF = StallD = FlushE = lwstall     
    wire lwstall;               
    assign lwstall = ( (rsD==rtE) | (rtD==rtE) ) && memtoregE;

    
//branch
// forwarding logic
//  ForwardAD = (rsD !=0) AND (rsD == WriteRegM) AND RegWriteM
//	ForwardBD = (rtD !=0) AND (rtD == WriteRegM) AND RegWriteM
    assign forwardaD = (rsD !=0) & (rsD == writeregM) & regwriteM;
    assign forwardbD = (rtD !=0) & (rtD == writeregM) & regwriteM;


// Stalling logic:
//	branchstall = BranchD AND RegWriteE AND 
//                   (WriteRegE == rsD OR WriteRegE == rtD) 
//                 OR BranchD AND MemtoRegM AND 
//                   (WriteRegM == rsD OR WriteRegM == rtD)
//	StallF = StallD = FlushE = lwstall OR branchstall
    wire branchstall;
    assign branchstall = ( branchD & regwriteE & 
                   (writeregE == rsD | writeregE == rtD) )
                 | ( branchD & memtoregM & 
                   (writeregM == rsD | writeregM == rtD) );
	wire jrstall;
	assign jrstall = (jrD & regwriteE &  (writeregE == rsD| writeregM == rtD))|
					 (jrD & memtoregM & (writeregM == rsD| writeregM == rtD ) );

    assign stallF = (lwstall | branchstall | stall_div | jrstall) & ~flush_exception;   //-------------重点 ~flush_exception
    assign stallD = stallF;
    assign flushE = lwstall | branchstall | jrstall | flush_exception;//1'b0;
	assign flushD = flush_exception;
	assign flushM = flush_exception; 


    assign stallE = stall_div & ~flush_exception;  //div
	assign stallM = 1'b0 && ~flush_exception;//1'b0;  //div
    
endmodule
