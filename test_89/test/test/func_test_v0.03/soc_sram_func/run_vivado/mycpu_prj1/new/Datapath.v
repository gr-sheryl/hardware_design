`timescale 1ns / 1ps
`include"defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 13:01:57
// Design Name: 
// Module Name: Datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module datapath(
	input wire clk,rst,
	//fetch stage
	output wire[31:0] pcF,
	input wire[31:0] instrF,
	output wire stallF,
	//decode stage
	input wire pcsrcD,branchD,
	input wire jumpD,
	output wire equalD,
	output wire[5:0] opD,functD,
	output wire[4:0] rsD,rtD,rdD,
	//execute stage
	input wire memtoregE,
	input wire alusrcE,regdstE,
	input wire regwriteE,
	input wire[7:0] alucontrolE_temp,
	output wire flushE,
	output wire[4:0] rsE,rtE,rdE,
	//mem stage
	input wire memtoregM,
	input wire regwriteM,
	output wire[31:0] aluoutM,writedataM,
	input wire[31:0] readdataM,
	//writeback stage
	input wire memtoregW,
	input wire regwriteW,
	output wire[31:0] resultW,
	output wire[31:0] srcbE,
	output wire[31:0] srcb2E,
	output wire [31:0] aluoutE,
	output wire [63:0] hilo_OW,
	input wire jalD,jrD,balD,
	output wire [31:0] pcplus8E,
	output wire jumpE,jrE,jalE,balE,
	output wire [3:0] selM,
	output wire stallE,
	output wire [4:0] writeregW,
	output wire stallD,
	output wire [31:0]reg_true_dataW,
	output wire stallM,
	input wire [5:0] interrupt,
	output wire flushD,flushM
    );
	
		wire flush_exception;


	//FD
	wire [31:0] pcnextFD,pcnextbrFD,pcplus4F,pcbranchD;
	//decode stage
	wire [31:0] pcplus4D,instrD;
	wire forwardaD,forwardbD;
	
	wire [31:0] signimmD,signimmshD;
	wire [31:0] srcaD,srca2D,srcbD,srcb2D;
	//execute stage
	wire [1:0] forwardaE,forwardbE;
	wire [4:0] writeregE;
	wire [31:0] signimmE;
	wire [31:0] srcaE,srca2E,srcb3E;
	//mem stage
	wire [4:0] writeregM;
	//writeback stage
	wire [31:0] aluoutW,readdataW;
    wire [1:0] addrW;
	wire [7:0] alucontrolW;
	wire stall_div;
	//hazard detection
	hazard h(
		//fetch stage
		stallF,
		//decode stage
		rsD,rtD,
		branchD,
		forwardaD,forwardbD,
		stallD,
		//execute stage
		rsE,rtE,
		writeregE,
		regwriteE,
		memtoregE,
		forwardaE,forwardbE,
		flushE,
		//mem stage
		writeregM,
		regwriteM,
		memtoregM,
		//write back stage
		writeregW,
		regwriteW,
		stall_div,
		stallE,
		jrD,
		stallM,
		flush_exception,
		flushD,flushM
		);

	//next PC logic (operates in fetch an decode)
	mux2 #(32) pcbrmux(pcplus4F,pcbranchD,pcsrcD,pcnextbrFD);



	//---------------------------------------------------
	wire [31:0] pc_jump;
	assign pc_jump = (jrD == 1) ? srca2D  :{pcplus4D[31:28],instrD[25:0],2'b00};	

	mux2 #(32) pcmux(pcnextbrFD,
		pc_jump,
		jumpD | jalD | jrD ,pcnextFD);
	//--------------------------------------------------
    
	//-------------------------is_delay_slot-------------------------
	wire jump_yn,is_in_delayslotD,is_in_delayslotE;
	assign jump_yn = jumpD | jalD | jrD | branchD;
	flopenrc #(32) delay_slotD(clk,rst,~stallD,flushE,jump_yn,is_in_delayslotD);
	flopenrc #(32) delay_slotE(clk,rst,~stallE,flushE,is_in_delayslotD,is_in_delayslotE);
	//-------------------------is_delay_slot-------------------------/

	//regfile (operates in decode and writeback)
	regfile rf(~clk,rst,regwriteW,rsD,rtD,writeregW,resultW,srcaD,srcbD,alucontrolW,addrW);

	//--------------------------exception_pc--------------------
	wire [31:0] new_pc,new_pc_clocks;
	wire [8:0] exception_signs;
	wire [31:0] true_pcnextFD;
	flopr #(32) test(clk,rst,new_pc,new_pc_clocks);
	assign  true_pcnextFD = (flush_exception) ? new_pc : pcnextFD ;

	//----------------------------------------------------------------/

	//fetch stage logic
	pc_new #(32) pc(clk,rst,~stallF,true_pcnextFD,pcF);
	adder pcadd1(pcF,32'b100,pcplus4F);
	//decode stage
	flopenrc #(32) r1D(clk,rst,~stallD,flushD,pcplus4F,pcplus4D);
	flopenrc #(32) r2D(clk,rst,~stallD,flushD,instrF,instrD);
	signext se(instrD[15:0],signimmD);
	sl2 immsh(signimmD,signimmshD);
	adder pcadd2(pcplus4D,signimmshD,pcbranchD);
	mux2 #(32) forwardamux(srcaD,aluoutM,forwardaD,srca2D);
	mux2 #(32) forwardbmux(srcbD,aluoutM,forwardbD,srcb2D);
	eqcmp comp(opD,rtD,srca2D,srcb2D,equalD);

	assign opD = instrD[31:26];
	assign functD = instrD[5:0];
	assign rsD = instrD[25:21];
	assign rtD = instrD[20:16];
	assign rdD = instrD[15:11];




	//execute stage
	flopenrc #(32) r1E(clk,rst,~stallE,flushE,srcaD,srcaE);
	flopenrc #(32) r2E(clk,rst,~stallE,flushE,srcbD,srcbE);
	flopenrc #(32) r3E(clk,rst,~stallE,flushE,signimmD,signimmE);
	flopenrc #(5) r4E(clk,rst,~stallE,flushE,rsD,rsE);
	flopenrc #(5) r5E(clk,rst,~stallE,flushE,rtD,rtE);
	flopenrc #(5) r6E(clk,rst,~stallE,flushE,rdD,rdE);


	//------------------------------------------------
	flopenrc #(1) r7E(clk,rst,~stallE,flushE,jumpD,jumpE);
	flopenrc #(1) r8E(clk,rst,~stallE,flushE,jrD,jrE);
	flopenrc #(1) r9E(clk,rst,~stallE,flushE,jalD,jalE);
	flopenrc #(32) r10E(clk,rst,~stallE,flushE,pcplus4D+{{29{1'b0}},3'b100},pcplus8E);
	flopenrc #(1) r11E(clk,rst,~stallE,flushE,balD,balE);
	//------------------------------------------------/



	mux3 #(32) forwardaemux(srcaE,resultW,aluoutM,forwardaE,srca2E);
	mux3 #(32) forwardbemux(srcbE,resultW,aluoutM,forwardbE,srcb2E);
	mux2 #(32) srcbmux(srcb2E,signimmE,alusrcE,srcb3E);


	//---------------interrupt-------------------------


	wire  exception_PC_F,exception_PC_D, exception_PC_E;
	wire [7:0] alucontrolE;
	wire [31:0] instrE;
	assign exception_PC_F = (pcF[1:0] != 2'b00);

	flopenrc #(32) instr_E(clk,rst,~stallE,flushE,instrD,instrE);

	flopenrc #(1) intD(clk,rst,~stallD,flushD,exception_PC_F,exception_PC_D);
	flopenrc #(1) intE(clk,rst,~stallE,flushE,exception_PC_D,exception_PC_E);

	assign alucontrolE  = (instrE == `EXE_ERET)? `EXE_ERET_OP  : alucontrolE_temp;

	//---------------interrupt------------------------/



	wire [4:0] sa;
	wire [1:0] addrE;
	wire [3:0] selE;
	assign sa = signimmE[10:6];
	wire [63:0] hilo_E,hilo_M,hilo_true;
	wire [31:0] reg_hi,reg_lo;
	alu alu_div(clk,rst,srca2E,srcb3E,sa,alucontrolE,aluoutE,hilo_OW,addrE,selE,stall_div,
	exception_PC_E,1'b1,exception_signs);

	flopenrc #(64) r_hiloM(clk,rst,~stallM,flushM,hilo_OW,hilo_M);
	hilo_reg HILO(clk,rst,we,hilo_M[63:32],hilo_M[31:0],reg_hi,reg_lo);
	
	//----------------------------------------------------------------
	wire [31:0] pcD,pcE;
	flopenrc #(32) pc_D(clk,rst,~stallD,flushD,pcF,pcD);
	flopenrc #(32) pc_E(clk,rst,~stallE,flushE,pcD,pcE);
	wire [31:0] rd_dataM;

	exception excep(clk,rst,pcE,exception_PC_E,is_in_delayslotE,interrupt,exception_signs,rdE,srca2E+srcb3E,srcb3E,flush_exception,new_pc,rd_dataM);
	//----------------------------------------------------------------
	

	mux2 #(5) wrmux(rtE,rdE,regdstE,writeregE);
	//mem stage
	flopenrc #(32) r1M(clk,rst,~stallM,flushM,srcb2E,writedataM);



	//-----------------------------------------------------------
	wire [31:0] aluout_EM;
	wire [4:0] writereg_EM;
	wire [7:0] alucontrolM;
	wire [1:0] addrM;
	assign aluout_EM = (balE == 1'b1 | jalE ==1'b1 | (jumpE == 1'b0 && jrE == 1'b1 ) )? pcplus8E : 
					   (  exception_signs[0] ) ? rd_dataM : aluoutE;
	assign writereg_EM = (balE == 1'b1 | jalE ==1'b1 ) ? 5'b11111 : 
						 (jumpE == 1'b0 && jrE == 1'b1 && rdE == 5'b00000 ) ?  5'b11111:
						 (jumpE == 1'b0 && jrE == 1'b1 && rdE != 5'b00000 ) ? rdE  :  writeregE;
	wire [31:0] aluoutM_temp;
	wire mfc0_ok;
	flopenrc #(32) r2M(clk,rst,~stallM,flushM,aluout_EM,aluoutM);
	flopenrc #(5) r3M(clk,rst,~stallM,flushM,writereg_EM,writeregM);
	flopenrc #(8) r4M(clk,rst,~stallM,flushM,alucontrolE,alucontrolM);
	flopenrc #(4) r5M(clk,rst,~stallM,flushM,selE,selM);
	flopenrc #(2) r6M(clk,rst,~stallM,flushM,addrE,addrM);
//	flopenrc #(1) r7M(clk,rst,~stallM,flushM,exception_signs[0],mfc0_ok);

//	assign   aluoutM = (mfc0_ok) ?rd_dataM : aluoutM_temp;
	//-----------------------------------------------------------

	//writeback stage

	flopr #(32) r1W(clk,rst,aluoutM,aluoutW);
	flopr #(32) r2W(clk,rst,readdataM,readdataW);
	flopr #(5) r3W(clk,rst,writeregM,writeregW);
	flopr #(2) r4W(clk,rst,addrM,addrW);
	flopr #(8) r5W(clk,rst,alucontrolM,alucontrolW);

	wire [31:0] resultW_temp;
	mux2 #(32) resmux(aluoutW,readdataW,memtoregW,resultW);
	assign resultW_temp = resultW;
	reg_data wdata(resultW_temp,addrW,alucontrolW,reg_true_dataW);
endmodule
