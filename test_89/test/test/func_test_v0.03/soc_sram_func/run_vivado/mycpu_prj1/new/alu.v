`timescale 1ns / 1ps
`include "defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 17:42:04
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu
(
    input clk,
    input rst,
    input [31:0] numA,//aluA
    input [31:0] numB,//aluB
    input [4:0] sa,
    input [7:0] op, 
    output reg [31:0] result,
    output reg [63:0] hilo_OW,
    output wire [1:0] addr,
    output reg [3:0] sel,
    output reg  stall_div,
    input wire exc_pcE,alu_ok,
    output wire [8:0] exception_signs
);

wire [31:0] mult_a,mult_b;

assign mult_a = (numA[31] == 1'b0) ? numA : (~numA + 1);
assign mult_b = (numB[31] == 1'b0) ? numB : (~numB + 1);


reg start_div,signed_div;
wire div_ready;
wire [63:0]hilo_div;


wire [31:0] div_A,div_B;
assign div_A = (start_div == 1'b0) ? numA :div_A;
assign div_B = (start_div == 1'b0) ? numB :div_B;
assign addr = numA[1:0] + numB[1:0]; 

wire check;

assign check  =  ~ ( (numA[1:0] == 2'b00 & numB[1:0] == 2'b00 ) | (numA[1:0] == 2'b10 & numB[1:0] == 2'b10 ) |
                     (numA[1:0] == 2'b01 & numB[1:0] == 2'b11 ) | (numA[1:0] == 2'b11 & numB[1:0] == 2'b01 ) );

//-------------------------interrupt/exception--------------------------
wire instr_error,overflow,addr_load_exc,addr_store_exc,mtc0,mfc0,eret,syscall,breakE;
assign overflow = ( (op == `EXE_ADD_OP | op == `EXE_ADDI_OP) & ( (numA[31] & numB[31] & ~result[31]) | (~numA[31] & ~numB[31] & result[31]) ) ) |
                  ( op == `EXE_SUB_OP & (numA[31] & !numB[31] & !result[31] | !numA[31] & numB[31] & result[31]) );

assign addr_load_exc =  (op == `EXE_LW_OP & check) | ( (op == `EXE_LHU_OP | op == `EXE_LH_OP) & numA[0] != numB[0] );
assign addr_store_exc = (op == `EXE_SW_OP & check ) | (op == `EXE_SH_OP & numA[0] != numB[0] );

assign mtc0 = (op == `EXE_MTC0_OP);
assign mfc0 = (op == `EXE_MFC0_OP);

assign instr_error = (op == 8'b1111_1111);

assign eret = (op == `EXE_ERET_OP);

assign syscall = (op == `EXE_SYSCALL_OP);

assign breakE = (op == `EXE_BREAK_OP);

assign exception_signs = (rst) ? 9'b000000000 :{addr_load_exc,instr_error,overflow,breakE,syscall,eret,addr_store_exc,mtc0,mfc0};

//-------------------------interrupt/exception--------------------------



always @(*) begin
  if (rst) begin
    result<=32'h00000000; 
    stall_div <= 1'b0;
    signed_div <= 1'b0;
    start_div <= 1'b0;
    hilo_OW <= 0;    
  end
end


    always @(*) begin

        case(op)
            //----------------------logit-------------------------------------8
            `EXE_AND_OP:  result <= numA & numB;
            `EXE_ANDI_OP: result <= numA & {{16{1'b0}},numB[15:0]}; 
            `EXE_OR_OP:   result <= numA | numB;
            `EXE_ORI_OP:  result <= numA | {{16{1'b0}},numB[15:0]};
            `EXE_XOR_OP:  result <= numA ^ numB;
            `EXE_XORI_OP: result <= (numA ^ {{16{1'b0}},numB[15:0]}); 
            `EXE_NOR_OP:  result <= ~(numA | numB);
            `EXE_LUI_OP:  result <= {numB[15:0],{16{1'b0}}};
            //----------------------logit-------------------------------------

            //----------------------shift-------------------------------------6
            `EXE_SLL_OP:  result <= numB << sa;
            `EXE_SRL_OP:  result <= numB >> sa;
            `EXE_SRA_OP:  result <= ( {32{numB[31]}} << (6'd32 - {1'b0,sa}) ) | numB >> sa;
            `EXE_SLLV_OP: result <= numB << numA[4:0];
            `EXE_SRLV_OP: result <= numB >> numA[4:0];
            `EXE_SRAV_OP: result <= ( {32{numB[31]}} << (6'd32 - {1'b0,numA[4:0]}) ) | numB >> numA[4:0];
            //----------------------shift-------------------------------------

            //----------------------HILO-------------------------------------4
            `EXE_MFHI_OP: begin result <= hilo_OW[63:32];   end  //HI写入rd
            `EXE_MTHI_OP: begin result <= 0; if (!exc_pcE) begin  hilo_OW[63:32] = numA; end  end  //rs写入HI
            `EXE_MFLO_OP: begin result <= hilo_OW[31:0];  end  //LO写入rd
            `EXE_MTLO_OP: begin result <= 0; if (!exc_pcE) begin hilo_OW[31:0] = numA;   end  end  //rs写入LO
            //----------------------HILO-------------------------------------

            //--------------------arithmetic-------------------14
            // U = unsigned 
            `EXE_SLT_OP:  result <= numA[31] & !numB[31] ? 1 : // a[31]: a<0
                              !numA[31] &  numB[31] ? 0 :
                               numA < numB;
            `EXE_SLTU_OP: result <= numA < numB;

            `EXE_ADD_OP:  result <= numA + numB;
            `EXE_ADDU_OP: result <= numA + numB;
            `EXE_SUB_OP:  result <= numA + (~numB+1);
            `EXE_SUBU_OP: result <= numA - numB;
            `EXE_MULT_OP: begin 
                result = 0; 
                hilo_OW <= (numA[31] == numB[31]) ? mult_a * mult_b : (~(mult_a * mult_b) + 1); 
            end 

            `EXE_MULTU_OP:begin 
                result = 0; 
                hilo_OW <= numA * numB;  
            end

            `EXE_DIV_OP:  begin 
                result = 0; 
                if (div_ready == 1'b0) begin
                    start_div <= 1'b1; 
                    signed_div <= 1'b1;  //1为有符号除法
                    stall_div <= 1'b1;
                end else if (div_ready == 1'b1) begin
                    start_div <= 1'b0;
                    signed_div <= 1'b1;
                    stall_div <= 1'b0;
                    hilo_OW <= hilo_div;
                end
                else begin
                    start_div <= 1'b0;
                    signed_div <= 1'b0;
                    stall_div <= 1'b0;
                end
                
            end

            `EXE_DIVU_OP: begin 
                result = 0; 
                if (div_ready == 1'b0) begin
                    start_div <= 1'b1; 
                    signed_div <= 1'b0;  //0为无符号除法
                    stall_div <= 1'b1;
                end else if (div_ready == 1'b1) begin
                    start_div <= 1'b0;
                    signed_div <= 1'b0;
                    stall_div <= 1'b0;
                    hilo_OW <= hilo_div;
                end
                else begin
                    start_div <= 1'b0;
                    signed_div <= 1'b0;
                    stall_div <= 1'b0;
                end
            end

            `EXE_ADDI_OP: result <= numA + numB;
            `EXE_ADDIU_OP:result <= numA + numB;
            `EXE_SLTI_OP: result <= numA[31] & !numB[31] ? 1 : // a[31]: a<0
                              !numA[31] &  numB[31] ? 0 :
                               numA < numB;

            `EXE_SLTIU_OP:result <= numA < numB;
            //--------------------arithmetic-------------------


            //-------------------load/store-------------------------------
            `EXE_LB_OP,`EXE_LH_OP,`EXE_LW_OP,`EXE_LHU_OP,`EXE_LBU_OP:
            begin
              result <= ((numA+numB) >> 2 ) << 2 ;
              sel <= 4'b0000;
            end

                `EXE_SB_OP:  begin
                    result <= ((numA+numB) >> 2 ) << 2 ;
                  if (addr == 2'b11)
                    begin   sel <= 4'b1000;   end
                  else if (addr == 2'b10)
                    begin   sel <= 4'b0100;   end
                  else if (addr == 2'b01)
                    begin   sel <= 4'b0010;   end
                  else if (addr == 2'b00)
                    begin   sel <= 4'b0001;   end
                  else
                    begin   sel <= 4'b0000;   end
                end
                `EXE_SH_OP: begin
                    result <= ((numA+numB) >> 2 ) << 2 ;
                  if (addr == 2'b10)
                    begin   sel <= 4'b1100;   end
                  else if (addr == 2'b00)
                    begin   sel <= 4'b0011;   end
                  else
                    begin   sel <= 4'b0000;   end
                end
                `EXE_SW_OP: begin  
                    result <= ((numA+numB) >> 2 ) << 2 ;
                    sel <= 4'b1111;  
                    end

                
            
            //----------------------load/store--------------------------------

            default:begin result<=32'h00000000; sel <= 4'b0000; end
            endcase
    end
  
    div divid(clk,rst,signed_div,div_A,div_B,start_div,1'b0,hilo_div,div_ready);



endmodule
