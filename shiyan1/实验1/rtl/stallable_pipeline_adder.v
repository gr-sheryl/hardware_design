`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/02 14:46:06
// Design Name: 
// Module Name: stallable_pipeline_adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module stallable_pipeline_adder(
    input [7:0] cin_a,              //第一个数
    input [7:0] cin_b,              //第二个数
    input c_in,                     //低位进位
    input clk,                      //时钟
    input rst,                      //整条刷新
    input validin,                  //输入信号
    input out_allow,                //输出信号
    input stop1,                     //暂停
    input stop2,
    input stop3,
    input stop4,
    input rst2,                     //刷新
    input rst3,
    input rst4,
    output validout,                //输出信号
    output reg c_out,               //高位进位
    output reg [7:0] sum_out        //输出和
    );
    reg c_out_t1, c_out_t2, c_out_t3;
    reg [1:0] sum_out_t1;
    reg [3:0] sum_out_t2;
    reg [5:0] sum_out_t3;
    
    reg pipe1_valid, pipe2_valid, pipe3_valid, pipe4_valid;     //第n级流水线上的有效数据
    
    //pipel stage1
    wire pipe1_allowin;         //第n级流水线可以收到上一级流水线的数据；
    wire pipe1_ready_go;        //第n级流水线可以流通，当前是否阻塞
    wire pipe1_to_pipe2_valid;  //第n级流水线和第n+1级流水线是否连通
    
    assign pipe1_ready_go=!stop1&&!stop2&&!stop3&&!stop4;
    assign pipe1_allowin=!pipe1_valid || pipe1_ready_go && pipe2_allowin;       //如果当前数据无效 OR 可以流通到下一级，就能接受有效数据
    assign pipe1_to_pipe2_valid= pipe1_valid && pipe1_ready_go;                 //当前数据有效并且可以流通到下一级，信号有效可以流通
    
    always @(posedge clk) begin
        if(rst ) begin
            pipe1_valid <= 1'b0;
        end
        else if(pipe1_allowin) begin
            pipe1_valid <= validin;
        end
        if( validin && pipe1_allowin) begin
            {c_out_t1,sum_out_t1} <= {1'b0, cin_a[1:0]} + {1'b0, cin_b[1:0]} + c_in; 
        end
    end
    
    //pipe2 stage2    
    wire pipe2_allowin;
    wire pipe2_ready_go;
    wire pipe2_to_pipe3_valid;
    
    assign pipe2_ready_go=!stop2&&!stop3&&!stop4;
    assign pipe2_allowin=!pipe2_valid || pipe2_ready_go && pipe3_allowin;
    assign pipe2_to_pipe3_valid= pipe2_valid && pipe2_ready_go;
    always @(posedge clk) begin
        if(rst || rst2) begin
            pipe2_valid <= 1'b0;
        end
        else if(pipe2_allowin) begin
            pipe2_valid <= pipe1_to_pipe2_valid;
        end
        if( pipe1_to_pipe2_valid && pipe2_allowin) begin
            {c_out_t2,sum_out_t2} <= {{1'b0, cin_a[3:2]} + {1'b0, cin_b[3:2]} + c_out_t1,sum_out_t1}; 
        end
    end
    
    //pipe3 stage3
    wire pipe3_allowin;
    wire pipe3_ready_go;
    wire pipe3_to_pipe4_valid;
    
    assign pipe3_ready_go=!stop3&&!stop4;
    assign pipe3_allowin=!pipe3_valid || pipe3_ready_go && pipe4_allowin;
    assign pipe3_to_pipe4_valid= pipe3_valid && pipe3_ready_go;
    always @(posedge clk) begin
        if(rst || rst2 || rst3) begin
            pipe3_valid <= 1'b0;
        end
        else if(pipe3_allowin) begin
            pipe3_valid <= pipe2_to_pipe3_valid;
        end
        if( pipe2_to_pipe3_valid && pipe3_allowin) begin
            {c_out_t3,sum_out_t3} <= {{1'b0, cin_a[5:4]} + {1'b0, cin_b[5:4]} + c_out_t2,sum_out_t2}; 
        end
    end
    
    //pipe4 stage4
    wire pipe4_allowin;
    wire pipe4_ready_go;

    assign pipe4_ready_go=!stop4;
    assign pipe4_allowin=!pipe4_valid || pipe4_ready_go && out_allow;
    always @(posedge clk) begin
        if(rst || rst2 || rst3 || rst4) begin
            pipe4_valid <= 1'b0;
        end
        else if(pipe4_allowin) begin
            pipe4_valid <= pipe3_to_pipe4_valid;
        end
        if( pipe3_to_pipe4_valid && pipe4_allowin) begin
            {c_out,sum_out} <= {{1'b0, cin_a[7:6]} + {1'b0, cin_b[7:6]} + c_out_t3,sum_out_t3}; 
        end
    end
    assign validout=pipe4_valid && pipe4_ready_go;
endmodule
