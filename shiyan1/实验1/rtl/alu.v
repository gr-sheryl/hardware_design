`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/04/25 20:27:01
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu(
    input wire [7:0] num1,
    input wire [2:0] op,
    output reg [31:0] re
    );
    wire num2=32'b01;    
    always @(*) begin
        case(op)
            3'b000: begin
                re <= {24'b0,num1} + num2;
            end
            3'b001: begin
                re <= {24'b0,num1}  - num2;
            end
            3'b010: begin
                re <= {24'b0,num1} & num2;
            end
            3'b011: begin
                re <= {24'b0,num1} | num2;
            end
            3'b100: begin
                re <= ~{24'b0,num1};
            end
            3'b101: begin
                re <= ({24'b0,num1} < num2);
            end
            default : begin
                re <= 32'b0;
            end
        endcase
    end
endmodule
