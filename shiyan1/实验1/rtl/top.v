`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/04/25 21:01:05
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input clk,
    input rst,
    input [7:0] num1,
    input [2:0] op,
    output [7:0] ans,       //8个数字
    output [6:0] seg        //小数点
    );
    wire [31:0] s;
    alu u1(.num1(num1),
    .op(op),
    .re(s));
    
    display u2(.clk(clk),
    .reset(rst),
    .s(s),
    .seg(seg),
    .ans(ans));
endmodule
