`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/01 17:33:44
// Design Name: 
// Module Name: a_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module a_sim(

    );
    reg [7:0] num1;
    wire [31:0] result;
    reg [2:0] op;    
    initial begin
        op=3'b000;
        num1=8'b00000010;
        #10  begin
        op=3'b001;
        num1=8'b11111111;
        end
        #10 begin
        op=3'b010;
        num1=8'b11111110;
        #10 begin
        op=3'b011;
        num1=8'b10101010;
        end
        #10 begin
        op=3'b100;
        num1=8'b11110000;
        end
        #10 begin
        op=3'b101;
        num1=8'b10000001;
        end
        end
    end
    
alu ut(
    .num1(num1),
    .op(op),
    .re(result)
    ); 
endmodule

