`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/02 22:56:19
// Design Name: 
// Module Name: stall_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module stall_sim(

    );
    reg [7:0] cin_a;
    reg [7:0] cin_b;
    reg c_in;
    reg clk;
    reg rst;
    reg validin;
    reg out_allow;
    reg stop1;
    reg stop2;
    reg stop3;
    reg stop4;
    reg rst2;
    reg rst3;
    reg rst4;
    wire validout;
    wire c_out;
    wire [7:0] sum_out;
    
    stallable_pipeline_adder us(
    .cin_a(cin_a),
    .cin_b(cin_b),
    .c_in(c_in),
    .clk(clk),
    .rst(rst),
    .validin(validin),
    .out_allow(out_allow),
    .stop1(stop1),
    .stop2(stop2),
    .stop3(stop3),
    .stop4(stop4),
    .rst2(rst2),
    .rst3(rst3),
    .rst4(rst4),
    .validout(validout),
    .c_out(c_out),
    .sum_out(sum_out)
    );
    initial begin

        cin_a=8'b00010010;
        cin_b=8'b01010011;
        c_in=1'b0;
        clk=1'b1;
        rst=1'b0;
        validin=1'b1;
        out_allow=1'b1;
        stop1=1'b0;
        stop2=1'b0;
        stop3=1'b0;
        stop4=1'b0;
        rst2=1'b0;
        rst3=1'b0;
        rst4=1'b0;
        #200 stop2=1'b1;           //10周期后流水线暂停
        #40 stop2=1'b0;          //暂停一个周期后恢复流动
        #80 rst3=1'b1;              //流水线刷新
        #20 rst3=1'b0;
    end
    always #10 clk=~clk;       //20ns 一个周期
endmodule
