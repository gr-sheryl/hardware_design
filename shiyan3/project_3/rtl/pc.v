`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/11 09:44:12
// Design Name: 
// Module Name: pc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pc(
    input clk,
    input rst,
    input [31:0] pcin,
    output reg [31:0] pcout
    );
   
    always @(negedge clk or posedge rst) begin
        if(rst) begin
            pcout <= 32'b0;
        end
        else begin
            pcout <= pcin;
        end
    end 
  

endmodule
