`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/11/07 13:50:53
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
	input  clk,rst,
	output  memwrite,  //写使能
	output [31:0] writedata,//写数据
	output [31:0] aluout,//计算结果，地址
	output [31:0] pc,
	output [31:0] instr//指令
    );
	// wire clk;
   	wire [31:0] readdata;
    wire pcsrc,zero;
	mips mips(
	.clk(clk),
	.rst(rst),
	.pc(pc),       //这里进行pc加4
	.instr(instr),
	.memwrite(memwrite),
	.aluout(aluout),
	.writedata(writedata),
	.readdata(readdata),
	.pcsrc(pcsrc),
	.zero(zero)
	);
	inst_mem imem(
      .clka(clk),    // input wire clka
      .addra(pc),  // input wire [31 : 0] addra ,输入最开始的pc
      .douta(instr)  // output wire [31 : 0] douta,输出指令
	);
    data_mem dmem(
      .clka(clk),    // input wire clka
      .wea({4{memwrite}}),      // input wire [3 : 0] wea
      .addra(aluout),  // input wire [31 : 0] addra
      .dina(writedata),    // input wire [31 : 0] dina
      .douta(readdata)  // output wire [31 : 0] douta
     );
endmodule
