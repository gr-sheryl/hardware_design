`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/11 10:25:13
// Design Name: 
// Module Name: controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module controller(
	input wire[5:0] op,funct,
	input wire zero,
	output memtoreg,memwrite,pcsrc,alusrc,regdst,regwrite,jump,
    output [2:0] alucontrol
    );
    wire [1:0] aluop;
    wire branch;
    maindec md(
    .op(op),
    .jump(jump),
    .branch(branch), 
    .alusrc(alusrc), 
    .memwrite(memwrite), 
    .memtoreg(memtoreg),
    .regwrite(regwrite), 
    .regdst(regdst),
    .aluop(aluop)
    );
    
    aludec ad(
    .aluop(aluop),
    .funct(funct),
    .alucontrol(alucontrol)
    );
    
    assign pcsrc = branch & zero ;
endmodule
