`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/12 22:14:49
// Design Name: 
// Module Name: datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module datapath(
	input wire clk,rst,
	input wire memtoreg,pcsrc,
	input wire alusrc,regdst,
	input wire regwrite,jump,
	input wire[2:0] alucontrol,
	output wire zero,
	output wire[31:0] pc,
	input wire[31:0] instr,
	output wire[31:0] aluout,writedata,
	input wire[31:0] readdata
    );
    
    wire [4:0] writereg;
    wire [31:0] pcnext;
    wire [31:0] pcnextbr,pcplus4,pcbranch;
    wire [31:0] result;
    wire [31:0] srca,srcb;
    wire [31:0] signimm, signimmsh;
    
    //pc��ַ
    pc pcreg(clk,rst,pcnext,pc);          
    adder pcadd1(pc,32'h00000004,pcplus4);
    sl2 immsh(signimm,signimmsh);       //shft left
    adder pcadd2(pcplus4,signimmsh,pcbranch);     //pc ��֧��ַ
    mux2 #(32) pcbrmux(pcbranch,pcplus4,pcsrc,pcnextbr);     //�ж�pc����һ��ָ���ַ
    mux2 #(32) pcmux({pcplus4[31:28],instr[25:0],2'b00},pcnextbr,jump,pcnext);      //��ת��ַ
    
    //
     mux2 #(5) wrmux(instr[15:11],instr[20:16], regdst, writereg); 
    regfile regf(clk,regwrite,instr[25:21],instr[20:16],writereg, result,srca,writedata);   //�Ĵ���
         //д�Ĵ���ѡ��
    mux2 #(32) resmux( readdata,aluout,memtoreg, result);            //�Ĵ���д������Դ�洢��/alu-result
    signext extend(instr[15:0],signimm);        //����λ��չ
    
    mux2 #(32) srcbmux(signimm,writedata,alusrc,srcb);    //alu����b�˿�
    alu alu(srca,srcb,alucontrol,aluout,zero); //alu����
endmodule
