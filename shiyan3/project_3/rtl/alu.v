`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/11 09:32:25
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu
(
    input [31:0] num2,//aluA
    input [31:0] num1,//aluB
    input [2:0] op,
    output reg [31:0] result,
    output wire zero
);
    always @(*) begin
        case(op)
            3'b010:result=num2+num1;//a+b
            3'b110:begin result=num2-num1; 
                            end//a-b
            3'b000:result=num2 & num1;//a and b
            3'b001:result=num2 | num1;//a or b
            3'b011:result=~num2;//~a
            3'b111:result = num2 < num1;
            default:result=32'h00000000;
            endcase
    end
    assign zero=(result==0)?1'b1:1'b0;

endmodule


