`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 22:44:15
// Design Name: 
// Module Name: top_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_sim(

    );
	reg clk,rst;
	wire  memwrite;
	wire [31:0] writedata;
	wire [31:0] readdata;
	wire [31:0] aluout;
	wire [31:0] pc;
	wire [31:0] instr;
	wire pcsrc;
	wire zero;
	top dtop(clk,rst,memwrite, writedata, readdata,aluout, pc,instr,pcsrc,zero);
    initial begin
	   clk=1'b0;
	   rst=1'b1;
	   #5 rst=1'b0;   
	end
	always #5 clk=!clk;
endmodule
