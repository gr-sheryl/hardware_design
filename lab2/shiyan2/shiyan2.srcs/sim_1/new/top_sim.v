`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/09 10:46:41
// Design Name: 
// Module Name: top_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_sim(

    );
    
    reg clk;
    reg rst;
    wire [31:0] pc;
    wire jump, branch, alusrc, memwrite, memtoreg, regwrite, regdst;
    wire [2:0] alucontrol;    

    top_nodiv ut(
    .clk(clk),
    .rst(rst),
    .pc(pc),
    .jump(jump),
    .branch(branch), 
    .alusrc(alusrc), 
    .memwrite(memwrite), 
    .memtoreg(memtoreg), 
    .regwrite(regwrite), 
    .regdst(regdst),
    .alucontrol(alucontrol)
    );   
    
    initial begin
        clk=1'b1;
        rst = 1'b1;
        
       #10  rst = 1'b0;
    end
    always #5 clk=~clk;
endmodule
