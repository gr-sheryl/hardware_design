`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/01 19:40:27
// Design Name: 
// Module Name: pc_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pc_sim(

    );
    reg clk;
    reg rst;
    wire [31:0] pc;
    wire inst_ce;
    initial begin
        clk=1'b1;
        rst = 1'b0;
    end
    always #5 clk=~clk;
        pc p(
    .clk(clk),
    .rst(rst),
    .pc(pc),
    .inst_ce(inst_ce)
    ); 
endmodule
