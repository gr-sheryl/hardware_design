`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/01 19:56:45
// Design Name: 
// Module Name: clk_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clk_sim(

    );
    reg clk_100hz;
    wire clk_1hz;
    initial begin
        clk_100hz=1'b1;
    end
    always #5 clk_100hz=~clk_100hz;
    clk_div clkt(
    clk_100hz,
    clk_1hz
    );
endmodule
