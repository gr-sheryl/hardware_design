`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/01 20:49:20
// Design Name: 
// Module Name: top_nodiv
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_nodiv(
    input clk,
    input rst,
    output [31:0] pc,
    output jump, branch, alusrc, memwrite, memtoreg, regwrite, regdst,
    output [2:0] alucontrol
    );
    wire [31:0] pcnext;
    wire ena;
    wire [31:0] inst;
    
    pc p(
    .clk(clk),
    .rst(rst),
    .pcnext(pcnext),
    .pc(pc),
    .inst_ce(ena)
    );      
    
    adder pcplus4(
    .a(pc),
    .b(32'h00000004),
    .y(pcnext)
    );
    
    blk_mem_gen_0 instrom (
      .clka(clk),    // input wire clka
      .ena(ena),      // input wire ena
      .addra(pc),  // input wire [31 : 0] addra
      .douta(inst)  // output wire [31 : 0] douta
    );
    
    controller Controller(
    .inst(inst),
    .jump(jump),
    .branch(branch), 
    .alusrc(alusrc), 
    .memwrite(memwrite), 
    .memtoreg(memtoreg),
    .regwrite(regwrite), 
    .regdst(regdst),
    .alucontrol(alucontrol)
    );

endmodule
