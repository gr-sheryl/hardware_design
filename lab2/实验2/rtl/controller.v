`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/08 23:12:05
// Design Name: 
// Module Name: controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module controller(
    input [31:0] inst,
    output jump, branch, alusrc, memwrite, memtoreg, regwrite, regdst,
    output [2:0] alucontrol
    );
    wire [1:0] aluop;
      
    main_decoder maindec(
    .op(inst[31:26]),
    .jump(jump),
    .branch(branch), 
    .alusrc(alusrc), 
    .memwrite(memwrite), 
    .memtoreg(memtoreg),
    .regwrite(regwrite), 
    .regdst(regdst),
    .aluop(aluop)
    );
    
    alu_decoder aludec(
    .aluop(aluop),
    .funct(inst[5:0]),
    .alucontrol(alucontrol)
    );
endmodule

