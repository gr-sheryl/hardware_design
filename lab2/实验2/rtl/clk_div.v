`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/28 16:38:10
// Design Name: 
// Module Name: clk_div
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clk_div(
    input clk_100hz,
    output clk_1hz
    );

    reg [25:0] counter = 26'b00000000000000000000000000;
    reg clk_out = 1'b0;
    always @(posedge clk_100hz) begin
        if (counter == 26'b10111110101111000010000000 ) begin
            counter <= 0;
            clk_out <= ~clk_out;
        end
        else begin
            counter <= counter + 1;
        end
    end
    assign clk_1hz = clk_out;
endmodule
