`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/09 10:08:59
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input clk_100hz,
    input rst,
    output jump, branch, alusrc, memwrite, memtoreg, regwrite, regdst,
    output [2:0] alucontrol,
    output [6:0]seg,
    output [7:0]ans   
    );
    
    wire clk;
    wire ena;
    wire [31:0] pc;
    wire [31:0] inst;
    wire [31:0] pcnext;
    clk_div CLK(
    .clk_100hz(clk_100hz),
    .clk_1hz(clk)
    );
  
    pc p(
    .clk(clk),
    .rst(rst),
    .pcnext(pcnext),
    .pc(pc),
    .inst_ce(ena)
    );      
    
    adder pcplus4(
    .a(pc),
    .b(32'h00000004),
    .y(pcnext)
    );
    
    blk_mem_gen_0 instrom (
      .clka(clk),    // input wire clka
      .ena(ena),      // input wire ena
      .addra(pc),  // input wire [31 : 0] addra
      .douta(inst)  // output wire [31 : 0] douta
    );
    
    controller Controller(
    .inst(inst),
    .jump(jump),
    .branch(branch), 
    .alusrc(alusrc), 
    .memwrite(memwrite), 
    .memtoreg(memtoreg),
    .regwrite(regwrite), 
    .regdst(regdst),
    .alucontrol(alucontrol)
    );
    
    display dis(
    .clk(clk_100hz),
    .reset(rst),
    .s(inst),
    .seg(seg),
    .ans(ans)
    );
endmodule
