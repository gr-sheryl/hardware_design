`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/08 23:09:20
// Design Name: 
// Module Name: main_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_decoder(
    input [5:0] op,
    output jump,
    output regwrite,
    output regdst,
    output alusrc,
    output branch,
    output memwrite,
    output memtoreg,
    output reg [1:0] aluop
    );
    reg [6:0] signs;
    assign {jump,regwrite,regdst,alusrc,branch,memwrite,memtoreg}= signs;
    always @ (*) begin
        case (op)    
            6'b000000: begin
                signs <= 7'b0110000;
                aluop <= 2'b10;
            end
            6'b100011: begin
                signs <= 7'b0101001;
                aluop <= 2'b00;
            end
            6'b101011: begin
                signs <= 7'b0001010;
                aluop <= 2'b00;
            end        
            6'b000100: begin
                signs <= 7'b0000100;
                aluop <= 2'b01;
            end        
            6'b001000: begin
                signs <= 7'b0101000;
                aluop <= 2'b00;
            end 
            6'b000010: begin
                signs <= 7'b1000000;
                aluop <= 2'b00;
            end      
            default: begin
                signs <= 7'b0000000;
                aluop <= 2'b00;
            end 
        endcase
    end
endmodule

