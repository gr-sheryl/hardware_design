`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/08 23:05:56
// Design Name: 
// Module Name: pc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pc(
    input clk,
    input rst,
    input [31:0] pcnext,
    output [31:0] pc,
    output inst_ce
    );
    reg [31:0] pcout;
    reg en;
    always @(posedge clk or posedge rst) begin
        if(rst) begin
            pcout <= 32'b0;
            en <= 1'b0;
        end
        else begin
            pcout <= pcnext;
            en <= 1'b1;
        end
    end
    assign pc = pcout;
    assign inst_ce = en;
endmodule

