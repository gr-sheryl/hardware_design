`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/05/08 23:10:50
// Design Name: 
// Module Name: alu_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu_decoder(
    input [1:0] aluop,
    input [5:0] funct,
    output reg [2:0] alucontrol
    );
    always @ (*) begin
        case (aluop)
            2'b00: begin            //lw/sw
                alucontrol <= 3'b010;
            end
            2'b01: begin            //beq
                alucontrol <= 3'b110;
            end
			default : case (funct)
				6'b100000:alucontrol <= 3'b010; //add
				6'b100010:alucontrol <= 3'b110; //sub
				6'b100100:alucontrol <= 3'b000; //and
				6'b100101:alucontrol <= 3'b001; //or
				6'b101010:alucontrol <= 3'b111; //slt
				default:  alucontrol <= 3'b000;
			endcase
        endcase
    end
endmodule

