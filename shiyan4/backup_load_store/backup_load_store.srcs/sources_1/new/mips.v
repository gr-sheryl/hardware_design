`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 12:55:33
// Design Name: 
// Module Name: mips
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mips(
	input wire clk,rst,
	output wire[31:0] pcF,
	input wire[31:0] instrF,
	output wire stallF, 
	output wire memwriteM,
	output wire[31:0] aluoutM,writedataM,
	output wire[31:0] resultW,
	output wire[31:0] srcbE,
	output wire[31:0] srcb2E,
	input wire[31:0] readdataM ,
	output wire[5:0] opD, 
	output wire[5:0] functD,  
	output wire pcsrcD,  
	output wire branchD,  
	output wire jumpD,  
	output wire memtoregE,  
	output wire alusrcE,  
	output wire[4:0] rsD,  
	output wire[4:0] rtD,  
	output wire[4:0] rdD,
	output wire[4:0] rsE,
	output wire[4:0] rtE,
	output wire[4:0] rdE,
	output wire [3:0] selM
    );
	
	wire memtoregM,regwriteM,regdstE,regwriteE,memtoregW,regwriteW;
	wire [7:0] alucontrolE;
	wire flushE,equalD;
	wire stallE;
	wire jalD,jrD,balD;
	controller c(
		clk,rst,
		//decode stage
		opD,functD,
		rtD,
		pcsrcD,branchD,equalD,jumpD,
		jalD,jrD,balD,
		//execute stage
		flushE,stallE,
		memtoregE,alusrcE,
		regdstE,regwriteE,	
		alucontrolE,

		//mem stage
		memtoregM,memwriteM,
		regwriteM,
		//write back stage
		memtoregW,regwriteW
		);
	datapath dp(
		clk,rst,
		//fetch stage
		pcF,
		instrF,
		stallF,
		//decode stage
		pcsrcD,branchD,
		jumpD,jalD,jrD,balD,
		equalD,
		opD,functD,
		rsD,rtD,rdD,
		//execute stage
		memtoregE,
		alusrcE,regdstE,
		regwriteE,
		alucontrolE,
		flushE,stallE,
		rsE,rtE,rdE,
		//mem stage
		memtoregM,
		regwriteM,
		aluoutM,writedataM,
		readdataM,selM,
		//writeback stage
		memtoregW,
		regwriteW,
		resultW,
		srcbE,
		srcb2E
	    );
	
endmodule