`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 17:43:34
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
	input wire clk,rst,
	output wire[31:0] writedata,
	output wire[31:0] dataadr,
	output wire memwrite,
	output wire[31:0] instr,
	output wire[31:0] pc,
	output wire[4:0] rsD,
	output wire[4:0] rtD,
	output wire[4:0] rdD,
	output wire[4:0] rsE,
	output wire[4:0] rtE,
	output wire[4:0] rdE,
	output wire pcsrcD,
	output wire branchD,
	output wire jumpD,
	output wire[5:0] opD,
	output wire[5:0] functD,
	output wire memtoregE,
	output wire alusrcE,
	output wire stallF
    );
    
    wire [31:0] readdata;
    wire [31:0] aluoutM;
    wire [3:0] sel2M,selM;
	wire [31:0] writedata_new;
	
	mips mips(.clk(clk),.rst(rst),.pcF(pc),.instrF(instr),.stallF(stallF),
	.aluoutM(dataadr),.memwriteM(memwrite),.writedataM(writedata),
	.readdataM(readdata),.opD(opD),.functD(functD),.pcsrcD(pcsrcD),
	.branchD(branchD),.jumpD(jumpD),.memtoregE(memtoregE),.alusrcE(alusrcE),
	.rsD(rsD),.rtD(rtD),.rdD(rdD),.rsE(rsE),.rtE(rtE),.rdE(rdE),.selM(selM));
	inst_mem imem(.clka(~clk),.addra(pc),.douta(instr)); 
	assign sel2M = (memwrite == 1'b0) ? 4'b0000 : selM;
	assign writedata_new = (sel_select == 4'b1100 | sel_select == 4'b0011) ? {2{writedata[15:0]}}  :
						(sel_select != 4'b0000 & sel_select != 4'b1111) ? {4{writedata[7:0]}} : writedata;
	
	data_mem dmem(.clka(clk),.ena( memwrite ),.wea(sel_select),.addra(dataadr),.dina(write_data),.douta(readdata)); //锟斤拷锟捷存储锟斤拷

endmodule
