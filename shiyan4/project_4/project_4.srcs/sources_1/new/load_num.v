`timescale 1ns / 1ps
`include"defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/01/03 16:19:51
// Design Name: 
// Module Name: load_num
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module load_num(
    input wire [31:0] readdata,
    input wire [7:0] aluop,
    input wire [1:0] addr,
    output reg [31:0] loaddata
    );
    always @(*) begin
        case (aluop)
            `EXE_LB_OP: begin
                case (addr)
                    2'b00: loaddata <= {{24{readdata[31]}},readdata[31:24]};
                    2'b01: loaddata <= {{24{readdata[23]}},readdata[23:16]};
                    2'b10: loaddata <= {{24{readdata[15]}},readdata[15:8]};
                    2'b11: loaddata <= {{24{readdata[7]}},readdata[7:0]}; 
                    default: loaddata <= readdata;
                endcase
            end 
            `EXE_LBU_OP: begin
                case (addr)
                    2'b00: loaddata <= {{24{0}},readdata[31:24]};
                    2'b01: loaddata <= {{24{0}},readdata[23:16]};
                    2'b10: loaddata <= {{24{0}},readdata[15:8]};
                    2'b11: loaddata <= {{24{0}},readdata[7:0]}; 
                    default: loaddata <= readdata;
                endcase
            end 
            `EXE_LH_OP: begin
                case (addr)
                    2'b00: loaddata <= {{16{readdata[31]}},readdata[31:16]};
                    2'b10: loaddata <= {{16{readdata[15]}},readdata[15:0]}; 
                    default: loaddata <= readdata;
                endcase
            end
            `EXE_LHU_OP: begin
                case (addr)
                    2'b00: loaddata <= {{16{0}},readdata[31:16]};
                    2'b10: loaddata <= {{16{0}},readdata[15:0]}; 
                    default: loaddata <= readdata;
                endcase
            end
            `EXE_LW_OP: loaddata <= readdata;
            default: loaddata <= readdata;
        endcase
    end
endmodule
