`timescale 1ns / 1ps
`include"defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 17:42:04
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu
(
    input clk,rst,

    input wire [31:0] a,//aluA
    input wire [31:0] b,//aluB
    input wire [7:0] op, 
    input wire [4:0] sa,
    output reg [31:0] y,
    output reg overflow,

    input wire [63:0] hilo_i,
    output reg [63:0] hilo_o,
    output wire hilo_write,
    output wire div_stall,
    output reg [3:0] sel

);
    reg [32:0] temp;
    //mult
    reg [31:0] temp_a;
    reg [31:0] temp_b;
    
    //div
    reg signed_div;
	reg start_div;

	wire[63:0]  result_div;
	wire ready_div;
    assign div_stall = (start_div == 1'b1) ? 1'b1: 1'b0;
    div dr(clk,rst,signed_div,a,b,start_div,1'b0,result_div,ready_div);

    assign hilo_write = (op == `EXE_MTHI_OP | op == `EXE_MTLO_OP) ? 1'b1: 1'b0;

    //store
    wire [31:0] offset;
    assign offset = {b[31:2], 2'b00};
    wire [1:0] addr = b[1:0];

    always @(*) begin
        if(rst) begin
            start_div <= 1'b0;
            signed_div <= 1'b0;
            hilo_o <= 64'b0;
        end else begin
        case(op)
            //logistic
            `EXE_AND_OP: y <= a & b;
            `EXE_OR_OP: y <= a | b;
            `EXE_XOR_OP: y <= a ^ b;
            `EXE_NOR_OP: y <= ~(a|b);
            `EXE_ANDI_OP: y <= a & {{16{1'b0}}, b[15:0]};
            `EXE_XORI_OP: y <= a ^ {{16{1'b0}}, b[15:0]};
            `EXE_LUI_OP: y <= {b[15:0],{16{1'b0}}};
            `EXE_ORI_OP: y <= a | {{16{1'b0}}, b[15:0]};

            //shift
            `EXE_SLL_OP: y <= b << sa;
            `EXE_SRL_OP: y <= b >> sa;
            `EXE_SRA_OP: y <= ({32{b[31]}} << (32-sa)) | (b >> sa);
            `EXE_SLLV_OP: y <= b << a[4:0];
            `EXE_SRLV_OP: y <= b >> a[4:0];
            `EXE_SRAV_OP: y <=({32{b[31]}} << (32-a[4:0])) | (b >> a[4:0]);
            
            //move
            `EXE_MFHI_OP: begin
                y <= hilo_i[63:32];
            end
            `EXE_MFLO_OP: begin
                y <= hilo_i[31:0];
            end
            `EXE_MTHI_OP: begin
                hilo_o <= {a,hilo_i[31:0]};
            end
            `EXE_MTLO_OP: begin 
                hilo_o <= {hilo_i[63:32],a};
            end

            //arithmetics

            //add/sub
            `EXE_ADD_OP: begin
                temp <= a + b;       
                overflow = (temp[32] == temp[31]) ? 0 : 1;    
                y <= temp[31:0]; 
            end
            `EXE_ADDU_OP: y <= a + b;

            `EXE_ADDI_OP: begin
                temp <= a + b;
                overflow = (temp[32] == temp[31]) ? 0 : 1;    
                y <= temp[31:0]; 
            end
            `EXE_ADDIU_OP: y <= a + b;

            `EXE_SUB_OP: begin
                temp <= a + (~b+1);       
                overflow = (temp[32] == temp[31]) ? 0 : 1;
                y <= temp[31:0]; 
            end
            `EXE_SUBU_OP: y <= a + (~b+1);
            //slt/sltu
            `EXE_SLT_OP: begin  
                temp_a <= a  - b;     
                y <= temp_a[31] == 1 ? 1 : 0;  

            end
            `EXE_SLTU_OP: begin
                temp <= {0,a} + (~{0,b} + 1);
                y <= temp[32] == 1 ? 1 : 0;  
            end
            `EXE_SLTI_OP: begin
                temp_a <= a  - b;     
                y <= temp_a[31] == 1 ? 1 : 0;  
            end
            `EXE_SLTIU_OP: begin
                temp <= {0,a} + (~{0,b} + 1);
                y <= temp[32] == 1 ? 1 : 0;  
            end
            //mult/multu
            `EXE_MULT_OP: begin
                //对负数取补码
                temp_a <= a[31] == 1'b1 ? (~a + 1) : a;
                temp_b <= b[31] == 1'b1 ? (~b + 1) : b;
                //修正结果，负数取补码
                hilo_o <= (a[31] ^ b[31] == 1'b1) ? ~(temp_a * temp_b) + 1 : temp_a * temp_b;
            end

            `EXE_MULTU_OP: begin
                hilo_o <= a * b;
            end
            //div/divu
            `EXE_DIV_OP: begin
                if(ready_div == 1'b0) begin                 //除法还未结束
                    start_div <= 1'b1;                      //除法开始了
                    signed_div <= 1'b1;                     //有符号数
                end 
                else if(ready_div == 1'b1) begin            //除法已经结束
                    start_div <= 1'b0;
                    signed_div <= 1'b1;
                end 
                else begin
                    start_div <= 1'b0;
                    signed_div <= 1'b0;
                end
                hilo_o <= result_div;
            end
            `EXE_DIVU_OP: begin
                if(ready_div == 1'b0) begin                 //除法还未结束
                    start_div <= 1'b1;
                    signed_div <= 1'b0;
                end 
                else begin                                  //除法已经结束
                    start_div <= 1'b0;
                    signed_div <= 1'b0;
                end 
                hilo_o <= result_div;
            end

            //load
            `EXE_LB_OP,`EXE_LH_OP,`EXE_LW_OP,`EXE_LHU_OP,`EXE_LBU_OP: begin
                y <= a + offset;
                sel <= 4'b0000;
            end
            `EXE_SB_OP: begin
                y <= a + offset;
                case (addr)
                    2'b00: sel <= 4'b1000;
                    2'b01: sel <= 4'b0100;
                    2'b10: sel <= 4'b0010;
                    2'b11: sel <= 4'b0001; 
                    default: sel <= 4'b0000;
                endcase
            end
            `EXE_SH_OP: begin
                y <= a + offset;
                case (addr)
                    2'b00: sel <= 4'b1100;
                    2'b10: sel <= 4'b0011; 
                    default: sel <= 4'b0000;
                endcase
            end
            `EXE_SW_OP: begin
                y <= a + offset;
                sel <= 4'b1111;                
            end
            default:    y <= 32'b0;
        endcase
        end
    end
    
endmodule
