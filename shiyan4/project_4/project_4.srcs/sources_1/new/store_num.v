`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/01/03 17:40:57
// Design Name: 
// Module Name: store_num
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module store_num(
    input wire [31:0] writedata,
    input wire [3:0] selM,
    input wire memwrite,
    output wire [3:0] sel2M,
    output wire [31:0] writedata_new
    );
    assign sel2M = (memwrite == 1'b0) ? 4'b0000 : selM;
	assign writedata_new = (sel2M == 4'b1100 | sel2M == 4'b0011) ? {2{writedata[15:0]}}  :
						(sel2M != 4'b0000 & sel2M != 4'b1111) ? {4{writedata[7:0]}} : writedata;
endmodule
