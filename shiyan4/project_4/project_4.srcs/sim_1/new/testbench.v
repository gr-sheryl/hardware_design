`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 18:06:13
// Design Name: 
// Module Name: top_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench(
    );
	reg clk=0;
	reg rst=0;

	wire[31:0] writedata,dataadr,pc,instr;
	wire[5:0] opD,functD;
	wire[4:0] rsD,rtD,rdD,rsE,rtE,rdE;
	wire memwrite,stallF,branchD,pcsrcD,jumpD,memtoregE,alusrcE;
	wire [39:0] ascii;
	
    instdec transfer(instr,ascii);
	top u(.clk(clk),.rst(rst),.writedata(writedata),.dataadr(dataadr),.memwrite(memwrite),
	.instr(instr),.pc(pc),.rsD(rsD),.rtD(rtD),.rdD(rdD),.rsE(rsE),.rtE(rtE),.rdE(rdE),
	.pcsrcD(pcsrcD),.branchD(branchD),.jumpD(jumpD),.opD(opD),.functD(functD),
	.memtoregE(memtoregE),.alusrcE(alusrcE),.stallF(stallF));
	

    always begin #10 clk = ~clk; end
	initial begin 
		rst = 1;
		#200;
		rst = 0;
	end
	// always @(negedge clk) begin
	// 	if(memwrite) begin
	// 		/* code */
	// 		if(dataadr === 84 & writedata === 7) begin
	// 			/* code */
	// 			$display("Simulation succeeded");
	// 			$stop;
	// 		end else if(dataadr !== 80) begin
	// 			/* code */
	// 			$display("Simulation Failed");
	// 			$stop;
	// 		end
	// 	end
	// end
	
endmodule

