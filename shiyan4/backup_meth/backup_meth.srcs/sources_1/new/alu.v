`timescale 1ns / 1ps
`include"defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 17:42:04
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu
(
    input clk,rst,

    input wire [31:0] a,//aluA
    input wire [31:0] b,//aluB
    input wire [7:0] op, 
    input wire [4:0] sa,
    output reg [63:0] hilo_o,
    output reg [31:0] y,
    output reg overflow,

	// input wire [63:0] result_div,
	// input wire div_ready,
    // output reg signed_div,
	// output reg[31:0] opdata1,
	// output reg[31:0] opdata2,
	// output reg start_div,
    output wire div_stall

);
    reg [32:0] temp;
    //mult
    reg [31:0] temp_a;
    reg [31:0] temp_b;
    
    //div
    reg signed_div;
	// reg[31:0] opdata1;
	// reg[31:0] opdata2;
	reg start_div;

	wire[63:0]  result_div;
	wire ready_div;
    assign div_stall = (start_div == 1'b1) ? 1'b1: 1'b0;

    div dr(clk,rst,signed_div,a,b,start_div,1'b0,result_div,ready_div);

    always @(*) begin
        if(rst) begin
            start_div <= 1'b0;
            signed_div <= 1'b0;
        end else begin
        case(op)
            //logistic
            `EXE_AND_OP: y <= a & b;
            `EXE_OR_OP: y <= a | b;
            `EXE_XOR_OP: y <= a ^ b;
            `EXE_NOR_OP: y <= ~(a|b);
            `EXE_ANDI_OP: y <= a & {{16{1'b0}}, b[15:0]};
            `EXE_XORI_OP: y <= a ^ {{16{1'b0}}, b[15:0]};
            `EXE_LUI_OP: y <= {b[15:0],{16{1'b0}}};
            `EXE_ORI_OP: y <= a | {{16{1'b0}}, b[15:0]};

            //shift
            `EXE_SLL_OP: y <= b << sa;
            `EXE_SRL_OP: y <= b >> sa;
            `EXE_SRA_OP: y <= ({32{b[31]}} << (32-sa)) | (b >> sa);
            `EXE_SLLV_OP: y <= b << a[4:0];
            `EXE_SRLV_OP: y <= b >> a[4:0];
            `EXE_SRAV_OP: y <=({32{b[31]}} << (32-a[4:0])) | (b >> a[4:0]);
            
            //move
            `EXE_MFHI_OP: y <= hilo_o[63:32];
            `EXE_MFLO_OP: y <= hilo_o[31:0];
            `EXE_MTHI_OP: hilo_o[63:32] <= a;
            `EXE_MTLO_OP: hilo_o[31:0] <= a;

            //arithmetics

            //add/sub
            `EXE_ADD_OP: begin
                temp <= a + b;       
                overflow = (temp[32] == temp[31]) ? 0 : 1;    
                y <= temp[31:0]; 
            end
            `EXE_ADDU_OP: y <= a + b;

            `EXE_ADDI_OP: begin
                temp <= a + b;
                overflow = (temp[32] == temp[31]) ? 0 : 1;    
                y <= temp[31:0]; 
            end
            `EXE_ADDIU_OP: y <= a + b;

            `EXE_SUB_OP: begin
                temp <= a + (~b+1);       
                overflow = (temp[32] == temp[31]) ? 0 : 1;
                y <= temp[31:0]; 
            end
            `EXE_SUBU_OP: y <= a + (~b+1);
            //slt/sltu
            `EXE_SLT_OP: begin  
                temp_a <= a  - b;     
                y <= temp_a[31] == 1 ? 1 : 0;  

            end
            `EXE_SLTU_OP: begin
                temp <= {0,a} + (~{0,b} + 1);
                y <= temp[32] == 1 ? 1 : 0;  
            end
            `EXE_SLTI_OP: begin
                temp_a <= a  - b;     
                y <= temp_a[31] == 1 ? 1 : 0;  
            end
            `EXE_SLTIU_OP: begin
                temp <= {0,a} + (~{0,b} + 1);
                y <= temp[32] == 1 ? 1 : 0;  
            end
            //mult/multu
            `EXE_MULT_OP: begin
                //对负数取补码
                temp_a <= a[31] == 1'b1 ? (~a + 1) : a;
                temp_b <= b[31] == 1'b1 ? (~b + 1) : b;
                //修正结果，负数取补码
                hilo_o <= (a[31] ^ b[31] == 1'b1) ? ~(temp_a * temp_b) + 1 : temp_a * temp_b;
            end

            `EXE_MULTU_OP: begin
                hilo_o <= a * b;
            end
            //div/divu
            `EXE_DIV_OP: begin
                if(ready_div == 1'b0) begin                 //除法还未结束
                    start_div <= 1'b1;                      //除法开始了
                    signed_div <= 1'b1;                     //有符号数
                end 
                else if(ready_div == 1'b1) begin            //除法已经结束
                    start_div <= 1'b0;
                    signed_div <= 1'b1;
                end 
                else begin
                    start_div <= 1'b0;
                    signed_div <= 1'b0;
                end
                hilo_o <= result_div;
            end
            `EXE_DIVU_OP: begin
                if(ready_div == 1'b0) begin                 //除法还未结束
                    start_div <= 1'b1;
                    signed_div <= 1'b0;
                end 
                else begin                                  //除法已经结束
                    start_div <= 1'b0;
                    signed_div <= 1'b0;
                end 
                hilo_o <= result_div;
            end
            default:    y <= 32'b0;
        endcase
        end
    end
    
endmodule
