`timescale 1ns / 1ps
`include "defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 18:03:57
// Design Name: 
// Module Name: maindec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module maindec(
	input wire[5:0] op,
    input wire [5:0] funct,
    input wire [4:0] rt,
	output wire memtoreg,memwrite,
	output wire branch,alusrc,
	output wire regdst,regwrite,
	output wire jump
    );
	reg [6:0] signs;
    assign {jump,regwrite,regdst,alusrc,branch,memwrite,memtoreg}= signs;
    always @ (*) begin
        case (op)  
            `EXE_NOP: begin
                signs <= 7'b0110000;
            end
            `EXE_LUI: begin
                signs <= 7'b0101000;
            end
            `EXE_ANDI: begin
                signs <= 7'b0101000;
            end
            `EXE_XORI: begin
                signs <= 7'b0101000;
            end        
            `EXE_ORI: begin
                signs <= 7'b0101000;
            end        
            `EXE_ADDI: begin
                signs <= 7'b0101000;
            end 
            `EXE_ADDIU: begin
                signs <= 7'b0101000;
            end    
            `EXE_SLTI: begin
                signs <= 7'b0101000;
            end
            `EXE_SLTIU: begin
                signs <= 7'b0101000;
            end
            `EXE_J: begin
                signs <= 7'b1000000;
            end
            `EXE_JAL: begin
                signs <= 7'b1000000;
            end
            `EXE_BEQ: begin
                signs <= 7'b0000100;
            end
            `EXE_BGTZ: begin
                signs <= 7'b0000100;
            end
            `EXE_BLEZ: begin
                signs <= 7'b0000100;
            end
            `EXE_BNE: begin
                signs <= 7'b0000100;
            end
            6'b000001: begin
                signs <= 7'b0000100;
            end
            `EXE_LB: begin
                signs <= 7'b0101001;
            end
            `EXE_LBU: begin
                signs <= 7'b0101001;
            end
            `EXE_LH: begin
                signs <= 7'b0101001;
            end
            `EXE_LHU: begin
                signs <= 7'b0101001;
            end
            `EXE_LW: begin
                signs <= 7'b0101001;
            end
            `EXE_SB: begin
                signs <= 7'b0001010;
            end
            `EXE_SH: begin
                signs <= 7'b0001010;
            end
            `EXE_SW: begin
                signs <= 7'b0001010;
            end
            default: begin
                signs <= 7'b0;
            end 
        endcase
    end
endmodule


