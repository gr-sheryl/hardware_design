`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 13:01:57
// Design Name: 
// Module Name: Datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module datapath(
	input wire clk,rst,
	//fetch stage
	output wire[31:0] pcF,
	input wire[31:0] instrF,
	output wire stallF,
	//decode stage
	input wire pcsrcD,branchD,
	input wire jumpD,jalD,jrD,balD,
	output wire equalD,
	output wire[5:0] opD,functD,
	output wire[4:0] rsD,rtD,rdD,
	//execute stage
	input wire memtoregE,
	input wire alusrcE,regdstE,
	input wire regwriteE,
	input wire[7:0] alucontrolE,
	output wire flushE,stallE,
	output wire[4:0] rsE,rtE,rdE,
	
	//mem stage
	input wire memtoregM,
	input wire regwriteM,
	output wire[31:0] aluoutM,writedataM,
	input wire[31:0] readdataM,
	//writeback stage
	input wire memtoregW,
	input wire regwriteW,
	output wire[31:0] resultW,
	output wire[31:0] srcbE,
	output wire[31:0] srcb2E
    );
	
	//FD
	wire [31:0] pcnextFD1,pcnextFD2,pcnextbrFD,pcplus4F,pcbranchD;
	//decode stage
	wire [31:0] pcplus4D,instrD;
	wire forwardaD,forwardbD;
	wire flushD,stallD,branchFlushD; 
	assign flushD = 1'b0;
	wire [4:0] saD;		//偏移量
	wire [31:0] signimmD,signimmshD;
	wire [31:0] srcaD,srca2D,srcbD,srcb2D;

	//execute stage
	wire [4:0] saE;
	wire [1:0] forwardaE,forwardbE;
	wire [4:0] writeregE, writereg2E;
	wire [31:0] signimmE;
	wire [31:0] srcaE,srca2E,srcb3E;
	wire [31:0] aluoutE,aluout2E;
	wire overflow;
	wire [31:0] pcplus8E;
	wire jalE,balE,jrE;
	//mem stage
	wire [4:0] writeregM;
	wire [63:0] hilo_o;	
	//writeback stage
	wire [4:0] writeregW;
	wire [31:0] aluoutW,readdataW;
	wire div_stall;

	//hilo
	wire hilo_writeE,hilo_writeM,hilo_writeW;
	wire [63:0] hilo_oW;
	wire [63:0] hilo_aluoutE,hilo_aluoutM,hilo_aluoutW;
	wire [63:0] hilo_aluin;
	wire [1:0] hilo_forward;

	//hazard detection
	hazard h(
		//fetch stage
		stallF,
		//decode stage
		rsD,rtD,
		branchD,balD,jumpD,
		forwardaD,forwardbD,
		stallD,branchFlushD,
		//execute stage
		rsE,rtE,
		writeregE,
		regwriteE,
		memtoregE,
		forwardaE,forwardbE,
		flushE,div_stall,
		stallE,
		//mem stage
		writeregM,
		regwriteM,
		memtoregM,
		//write back stage
		writeregW,
		regwriteW,
		//hilo
		hilo_writeE,
		hilo_writeM,
		hilo_writeW,
		hilo_forward
		);

	//next PC logic (operates in fetch an decode)
	mux2 #(32) pcbrmux(pcplus4F,pcbranchD,pcsrcD,pcnextbrFD);
	mux2 #(32) pcmux(pcnextbrFD,
		{pcplus4D[31:28],instrD[25:0],2'b00},
		(jumpD|jalD) && (!jrD),pcnextFD1);
	
	mux2 #(32) pcmux1(pcnextFD1,srca2D,jrD,pcnextFD2);		//jr跳转到rs

	//regfile (operates in decode and writeback)
	regfile rf(clk,regwriteW,rsD,rtD,writeregW,resultW,srcaD,srcbD);

	//fetch stage logic
	flopenr #(32) pc(clk,rst,~stallF,pcnextFD2,pcF);
	adder pcadd1(pcF,32'b100,pcplus4F);			//PC+4
	//decode stage
	flopenr #(32) r1D(clk,rst,~stallD,pcplus4F,pcplus4D);
	flopenrc #(32) r2D(clk,rst,~stallD,flushD,instrF,instrD);
	signext se(instrD[15:0],signimmD);
	sl2 immsh(signimmD,signimmshD);
	adder pcadd2(pcplus4D,signimmshD,pcbranchD);

	mux2 #(32) forwardamux(srcaD,aluoutM,forwardaD,srca2D);
	mux2 #(32) forwardbmux(srcbD,aluoutM,forwardbD,srcb2D);
	eqcmp comp(srca2D,srcb2D,opD,rtD,equalD);


	assign opD = instrD[31:26];
	assign functD = instrD[5:0];
	assign rsD = instrD[25:21];
	assign rtD = instrD[20:16];
	assign rdD = instrD[15:11];
	assign saD = instrD[10:6];

	//execute stage
	flopenrc #(32) r1E(clk,rst,~stallE,flushE,srcaD,srcaE);
	flopenrc #(32) r2E(clk,rst,~stallE,flushE,srcbD,srcbE);
	flopenrc #(32) r3E(clk,rst,~stallE,flushE,signimmD,signimmE);
	flopenrc #(5) r4E(clk,rst,~stallE,flushE,rsD,rsE);
	flopenrc #(5) r5E(clk,rst,~stallE,flushE,rtD,rtE);
	flopenrc #(5) r6E(clk,rst,~stallE,flushE,rdD,rdE);
	flopenrc #(5) r7E(clk,rst,~stallE,flushE,saD,saE);

	flopenrc #(1) r8E(clk,rst,~stallE,flushE,jalD,jalE);
	flopenrc #(1) r9E(clk,rst,~stallE,flushE,balD,balE);
	flopenrc #(1) r10E(clk,rst,~stallE,flushE,jrD,jrE);
	flopenrc #(32) r11E(clk,rst,~stallE,flushE,pcplus4D+32'b100,pcplus8E);

	//hilo
	mux2 #(64) forwardhilo(hilo_aluoutM,hilo_aluoutW,hilo_forward,hilo_aluin);

	mux3 #(32) forwardaemux(srcaE,resultW,aluoutM,forwardaE,srca2E);
	mux3 #(32) forwardbemux(srcbE,resultW,aluoutM,forwardbE,srcb2E);
	mux2 #(32) srcbmux(srcb2E,signimmE,alusrcE,srcb3E);
	alu alu(clk,rst,srca2E,srcb3E,alucontrolE,saE,aluoutE,overflow,hilo_aluin,hilo_aluoutE,hilo_writeE,div_stall);
	mux2 #(5) wrmux(rtE,rdE,regdstE,writeregE);
	mux2 #(5) wrmux2(writeregE,5'b11111,jalE|balE,writereg2E);		//jal /jalr写入地址
	mux2 #(32) wrmux3(aluoutE,pcplus8E,jalE|jrE|balE,aluout2E);		//jrE代指jalrE

	//mem stage
	flopr #(32) r1M(clk,rst,srcb2E,writedataM);
	flopr #(32) r2M(clk,rst,aluout2E,aluoutM);
	flopr #(5) r3M(clk,rst,writereg2E,writeregM);

	flopr #(64) r4M(clk,rst,hilo_aluoutE,hilo_aluoutM);
	flopr #(1) r5M(clk,rst,hilo_writeE,hilo_writeM);


	//writeback stage
	flopr #(32) r1W(clk,rst,aluoutM,aluoutW);
	flopr #(32) r2W(clk,rst,readdataM,readdataW);
	flopr #(5) r3W(clk,rst,writeregM,writeregW);
	mux2 #(32) resmux(aluoutW,readdataW,memtoregW,resultW);
	
	flopr #(64) r4W(clk,rst,hilo_aluoutM,hilo_aluoutW);
	flopr #(64) r5W(clk,rst,hilo_writeM,hilo_writeW);
 	hilo_reg hr(clk,rst,hilo_writeW,hilo_aluoutW[63:32],hilo_aluoutW[31:0],hilo_aluin[63:32],hilo_aluin[31:0]);


endmodule
