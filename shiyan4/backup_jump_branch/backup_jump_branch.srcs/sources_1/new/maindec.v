`timescale 1ns / 1ps
`include "defines.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 18:03:57
// Design Name: 
// Module Name: maindec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module maindec(
	input wire[5:0] op,
    input wire [5:0] funct,
    input wire [4:0] rt,
	output wire memtoreg,memwrite,
	output wire branch,alusrc,
	output wire regdst,regwrite,
	output wire jump,jal,jr,bal
    );
	reg [9:0] signs;
    assign {jump,regwrite,regdst,alusrc,branch,memwrite,memtoreg,jal,jr,bal}= signs;
    always @ (*) begin
        case (op)  
            `EXE_NOP: begin
                case (funct)
                    `EXE_JR: signs <= 10'b1000000010;    //jr
                    `EXE_JALR: signs <= 10'b0110000010;  //jalr
                    default: 
                        signs <= 10'b0110000000;
                endcase
            end
            `EXE_LUI: begin
                signs <= 10'b0101000000;
            end
            `EXE_ANDI: begin
                signs <= 10'b0101000000;
            end
            `EXE_XORI: begin
                signs <= 10'b0101000000;
            end        
            `EXE_ORI: begin
                signs <= 10'b0101000000;
            end        
            `EXE_ADDI: begin
                signs <= 10'b0101000000;
            end 
            `EXE_ADDIU: begin
                signs <= 10'b0101000000;
            end    
            `EXE_SLTI: begin
                signs <= 10'b0101000000;
            end
            `EXE_SLTIU: begin
                signs <= 10'b0101000000;
            end
            `EXE_J: begin
                signs <= 10'b1000000000;
            end
            `EXE_JAL: begin
                signs <= 10'b0100000100;
            end
            `EXE_BEQ: begin
                signs <= 10'b0000100000;
            end
            `EXE_BGTZ: begin
                signs <= 10'b0000100000;
            end
            `EXE_BLEZ: begin
                signs <= 10'b0000100000;
            end
            `EXE_BNE: begin
                signs <= 10'b0000100000;
            end
            `EXE_REGIMM_INST: begin
                case (rt)
                    `EXE_BLTZAL: signs <= 10'b0100100001;
                    `EXE_BGEZAL: signs <= 10'b0100100001;
                    default: signs <= 10'b0000100000;
                endcase
            end
            `EXE_LB: begin
                signs <= 10'b0101001000;
            end
            `EXE_LBU: begin
                signs <= 10'b0101001000;
            end
            `EXE_LH: begin
                signs <= 10'b0101001000;
            end
            `EXE_LHU: begin
                signs <= 10'b0101001000;
            end
            `EXE_LW: begin
                signs <= 10'b0101001000;
            end
            `EXE_SB: begin
                signs <= 10'b0001010000;
            end
            `EXE_SH: begin
                signs <= 10'b0001010000;
            end
            `EXE_SW: begin
                signs <= 10'b0001010000;
            end
            default: begin
                signs <= 10'b0;
            end 
        endcase
    end
endmodule