`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/20 17:43:34
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
	input wire clk,rst,
	output wire[31:0] writedata,
	output wire[31:0] dataadr,
	output wire memwrite,
	output wire[31:0] instr,
	output wire[31:0] pc,
	output wire[4:0] rsD,
	output wire[4:0] rtD,
	output wire[4:0] rdD,
	output wire[4:0] rsE,
	output wire[4:0] rtE,
	output wire[4:0] rdE,
	output wire pcsrcD,
	output wire branchD,
	output wire jumpD,
	output wire[5:0] opD,
	output wire[5:0] functD,
	output wire memtoregE,
	output wire alusrcE,
	output wire stallF
    );
    
    wire [31:0] readdata;
    wire [31:0] aluoutM;
    
	mips mips(.clk(clk),.rst(rst),.pcF(pc),.instrF(instr),.stallF(stallF),
	.aluoutM(dataadr),.memwriteM(memwrite),.writedataM(writedata),
	.readdataM(readdata),.opD(opD),.functD(functD),.pcsrcD(pcsrcD),
	.branchD(branchD),.jumpD(jumpD),.memtoregE(memtoregE),.alusrcE(alusrcE),
	.rsD(rsD),.rtD(rtD),.rdD(rdD),.rsE(rsE),.rtE(rtE),.rdE(rdE));
	inst_mem imem(.clka(~clk),.addra(pc),.douta(instr)); 
	data_mem dmem(.clka(clk),.wea(memwrite),.addra(dataadr),.dina(writedata),.douta(readdata));
	

endmodule
